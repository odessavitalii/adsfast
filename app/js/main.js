;(function() {
	'use strict';

	window.onload = function () {
        var formInput = document.querySelectorAll('.form .form__row .form__control');
        var nav = document.querySelector('.navigation');
        var navMob = document.querySelectorAll('.navigation-mob');

        formInput.forEach(function (input) {
            input.addEventListener('input', function (e) {
                if (e.target.value !== '') {
                    e.target.closest('.form__group').classList.add('_js-focus');
                } else {
                    e.target.closest('.form__group').classList.remove('_js-focus');
                }
            });
        });

        // if (document.querySelector('.swiper-container') && document.documentElement.clientWidth >= 1025 ) {
            var swiperPage = new Swiper('.swiper-container', {
                direction: 'vertical',
                slidesPerView: 1,
                spaceBetween: 0,
                speed: 600,
                parallax: true,
                keyboardControl: true,
                mousewheel: true,
                effect: "fade",
                pagination: false,
                navigation: false,
                simulateTouch: false,
                breakpoints: {
                    1024: {
                        simulateTouch: true,
                        mousewheel: false,
                    }
                },
                setTransition: {
                    transition: 150
                },
                on: {
                    slideNextTransitionEnd: function () {
                        detectSection();
                        // console.log('next')
                    },
                    slidePrevTransitionStart: function () {
                        detectSection();
                        // console.log('prev')
                    },
                    transitionStart: function () {
                        var section = document.querySelectorAll('.sec');

                        section.forEach(function (sec) {
                            if (sec.classList.contains('swiper-slide-next') || sec.classList.contains('swiper-slide-prev')) {
                                isAnimateRemove(sec, '.slideInLeftFadeIn');
                                isAnimateRemove(sec, '.slideInRightFadeIn');
                            }
                        })
                    },
                    slideChangeTransitionEnd: function () {
                        var section = document.querySelectorAll('.sec');

                        section.forEach(function (sec) {
                            if (sec.classList.contains('swiper-slide-active')) {

                                isAnimateAdd(sec, '.slideInLeftFadeIn');
                                isAnimateAdd(sec, '.slideInRightFadeIn');

                            } else {
                                isAnimateRemove(sec, '.slideInLeftFadeIn');
                                isAnimateRemove(sec, '.slideInRightFadeIn');
                            }
                        })
                    },
                    init: function () {
                        var section = document.querySelectorAll('.sec');

                        section.forEach(function (sec) {
                            if (sec.classList.contains('swiper-slide-active')) {
                                isAnimateAdd(sec, '.slideInLeftFadeIn');
                                isAnimateAdd(sec, '.slideInRightFadeIn');
                            }
                        })
                    }
                }
            });
        // }
        /* end swiper slider init */

        // if (document.documentElement.clientWidth >= 1025) {
            nav.addEventListener('click', function(e) {

                if (e.target.classList.contains('anchor')) {
                    e.preventDefault();

                    var attrNavlink = e.target.getAttribute('data-index');
                    swiperPage.slideTo(attrNavlink);
                }
            });
        // }

        navMob.forEach(function (elem) {
            elem.addEventListener('click', function(e) {

                if (e.target.classList.contains('navigation-btn-next')) {
                    swiperPage.slideNext();
                }

                if (e.target.classList.contains('navigation-btn-prev')) {
                    swiperPage.slidePrev();
                }
            });
        });

        function detectSection() {
            var navigation = document.querySelector('.navigation');
            var navigationItem = navigation.querySelectorAll('ul li');
            var indexSlide = swiperPage.activeIndex;
            var slides = swiperPage.slides;
            var slideAttr = slides[indexSlide].getAttribute('id');

            navigationItem.forEach(function (item) {
                var link = item.querySelector('a');
                var attr = link.getAttribute('href').slice(1);

                item.classList.remove('active');

                if (slideAttr === attr) {
                    item.classList.add('active')
                }
            });
        }

        /* function add Class "animated" for slider element */
        function isAnimateAdd(elem, amimationType) {
            elem.querySelectorAll(amimationType).forEach(function (item) {
                item.classList.add('animated');
            });
        }
        /* end function add Class for slider */

        /* function remove Class "animated" for slider */
        function isAnimateRemove(elem, amimationType) {
            elem.querySelectorAll(amimationType).forEach(function (item) {
                item.classList.remove('animated');
            });
        }
        /* end function remove Class for slider element */

        /* polyfill "closest" */
        (function (ElementProto) {
            if (typeof ElementProto.matches !== 'function') {
                ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
                    var element = this;
                    var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
                    var index = 0;

                    while (elements[index] && elements[index] !== element) {
                        ++index;
                    }

                    return Boolean(elements[index]);
                };
            }

            if (typeof ElementProto.closest !== 'function') {
                ElementProto.closest = function closest(selector) {
                    var element = this;

                    while (element && element.nodeType === 1) {
                        if (element.matches(selector)) {
                            return element;
                        }

                        element = element.parentNode;
                    }

                    return null;
                };
            }
        })(window.Element.prototype);
        /* end polyfill "closest" */
    };
})();
